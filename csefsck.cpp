/*
Dhishan Amaranath
N16909360

g++ csefsck.cpp -o csefsck -std=c++0x

To enable C++11 support with g++, you need to pass the option -std=c++0x.

./csefsck /Users/Dhishan/Documents/Scholastics/IntroOS/Project/FS
./csefsck /full/path/to/the/fusedata/folder
assuming all the free blocks contain all 4096 0's i.e till fusedata.9999

To verify the Free Block List
 - get the missing numbers from the free block list
 - open those blocks to check if they contain all 4096 0's then the missing number is not a valid filled block
*/



#include <iostream>
#include <stdio.h>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <time.h>
#include <vector>
using namespace std;

static string path;

struct filename_to_inode_dict{
  char type;
  string name;
  int location;
};

struct directory_inode{
	int size;
	int uid;
	int gid;
	int mode;
	long atime;
	long ctime;
	long mtime;
	int linkcount;
	vector<filename_to_inode_dict> inode_dict;
};

struct file_inode{
	int size;
	int uid;
	int gid;
	int mode;
	int linkcount;
	long atime;
	long ctime;
	long mtime;
	int indirect;
	int location;
};

int verifyDevID();
int verifyFreeBlock();
int verifyFreeBlockList(vector<int> &freeBlocks);
int getFreeBlockVector(ifstream& myfile,vector<int> &freeBlocks);
int verifyNonFreeBlocks(vector<int> &nonFreeBlocks);
int verifyDirectory();
int readDirectory(string fileName,struct directory_inode &dir_inode);
int verifyDotEntry(vector<filename_to_inode_dict> &list,int dotLoc,int dotdotLoc);
int findSubDirs(vector<filename_to_inode_dict> &full_list, vector<filename_to_inode_dict> &dir_list);
int verifyDirectoryEntries(int path_no,int root_dir);
int verifyLinkCnt(struct directory_inode &dir_inode);
int verifyTimes(long atime,long ctime,long mtime);
int findFiles(vector<filename_to_inode_dict> &full_list, vector<filename_to_inode_dict> &files);
int verifyFiles(int path_no);
int readFileInode(string fileName,struct file_inode &f_inode);
int getIndirectArray(int index_no,vector<int> &indexes);
int verifyFileSizeField(struct file_inode &f_inode,vector<int> &indexes);

void Tokenize(const string& str, vector<string>& tokens, const string& );

int main(int argc, char* argv[])
{
  path = argv[1];

  verifyDevID();
  verifyFreeBlock();
  verifyDirectory();

  return 0;
}

int verifyDevID(){
  string sbPath = path + "/fusedata.0" ;
  string data;
  cout << "Verifying Device ID and Time at SuperBlock: "<< sbPath << endl;
  long cTime;
  int mountedCnt;
  int devId;
  ifstream myfile;
  myfile.open(sbPath);
  if (!myfile.good())
    return 1; // exit if file not found
  if(myfile.is_open()){

    getline (myfile,data);
//    cout <<"string: " << data << endl;
  }
  data.erase( ::remove_if( data.begin(), data.end(), ::isspace ), data.end() );
  //cout <<"string: " << data << endl;
  sscanf(data.c_str(),"{creationTime:%ld, mounted:%d, devId:%d,",&cTime,&mountedCnt,&devId);

  if(cTime > (long)time(NULL))
  {
    cout << "Super Block Time is Invalid" << endl;
  }
  if(devId != 20)
  {
    cout << "Invalid DevId" << endl;
  }
  return 0;
}

///Verifies if all the free blocks mentioned in the free block list i.e all the files fusedata.27 to fusedata.9999
///if listed in the free block list contain only 0's
///Verifies for the blocks not listed in the free block doesnt contain all 4096 0's - i.e something exist
int verifyFreeBlock(){
  cout << "Verifying Free Blocks ..." << endl;
  cout << "assuming all the 10000 fusedata files exist" << endl;
  vector<int> freeBlocksFull;
  //Get the Free Block list in each freeblock list blocks and verify them to contain only 0's -- freeblock
  for(int i= 1;i<=25;i++){
  //for(int i= 1;i<=2;i++){
    string sbPath = path + "/fusedata." + ::to_string(i) ;
    //cout << sbPath << endl;

    vector<int> freeBlocks;
    ifstream myfile;
    myfile.open(sbPath);
    if (!myfile.good()){
      cout << "File Open Error: " << sbPath << endl;
      return 1;
    }
    getFreeBlockVector(myfile,freeBlocks);
    verifyFreeBlockList(freeBlocks); /// requires all the 10000 files //TBD: uncomment before submission
    freeBlocksFull.insert(freeBlocksFull.end(),freeBlocks.begin(),freeBlocks.end());
  }
  //For all the blocks which are not in the free block list verify their validity.
  //print the non free blocks.
  vector<int> nonFreeBlocks;
  int validFreeBlock = 27;
  for(vector<int>::const_iterator i = freeBlocksFull.begin(); i != freeBlocksFull.end(); ){
    if(validFreeBlock == *i){
      validFreeBlock++; i++;
    }else if(validFreeBlock < *i){
      nonFreeBlocks.push_back(validFreeBlock);
      validFreeBlock++;
    }
  }
  cout << "Non Free Blocks extracted from the Free Block List" << endl;
  for(vector<int>::const_iterator i = nonFreeBlocks.begin(); i!= nonFreeBlocks.end();++i)
    cout << ::to_string(*i) << endl;
  cout << "Verifying non Free Blocks" << endl;

  verifyNonFreeBlocks(nonFreeBlocks);
  return 0;
}

int verifyFreeBlockList(vector<int> &freeBlocks){
  for( vector<int>::const_iterator i = freeBlocks.begin(); i != freeBlocks.end(); ++i){
    string sbPath = path + "/fusedata." + ::to_string(*i) ;
    ifstream emptyFile;
    emptyFile.open(sbPath);
    if (!emptyFile.good()){
      cout << "File Open Error: " << sbPath << endl;
      return 1;
    }
    //if the file is free and is not any directory or file inode index or no written content. it contains 0's
    //compare if the file contain anything other than 0's then throw an error.
    string fileData;
    string compData = "0";
    int count = 0;
    ::string ::size_type pos = 0;
    while(getline(emptyFile,fileData)){
      while( (pos = fileData.find(compData,pos)) != ::string::npos){
        count ++;
        pos += compData.size();
      }
    }

    if(count != 4096)
      cout << "Error: Block - fusedata." << ::to_string(*i) << "is not free block - contains " << ::to_string(count) << "0's" <<endl;
    else cout << "Block: fusedata." << ::to_string(*i) << " is a verified free block" << endl;
  }
  //data.erase( ::remove_if( data.begin(), data.end(), ::isspace ), data.end() ); //remove white spaces
  return 0;
}

int getFreeBlockVector(ifstream& myfile,vector<int> &freeBlocks){

  string lineData;
  //read each line
  while(getline(myfile,lineData)){
    istringstream streamData(lineData);
    string element;
    while(getline(streamData,element,',')){
      freeBlocks.push_back(atoi(element.c_str()));
    }
  }
  return 0;
}

int verifyNonFreeBlocks(vector<int> &nonFreeBlocks){
  for(vector<int>::const_iterator i = nonFreeBlocks.begin(); i!=nonFreeBlocks.end() ; ++i){
    string sbPath = path + "/fusedata." + ::to_string(*i) ;
    ifstream emptyFile;
    emptyFile.open(sbPath);
    if (!emptyFile.good()){
      cout << "File Open Error: " << sbPath << endl;
      continue;
    }
    string fileData;
    string compData = "0";
    int count = 0;
    ::string ::size_type pos = 0;
    while(getline(emptyFile,fileData)){
      while( (pos = fileData.find(compData,pos)) != ::string::npos){
        count ++;
        pos += compData.size();
      }
    }
    if(count == 4096)
      cout << "Error: fusedata." << ::to_string(*i) << " is a free block  but not contained in the free Block List" <<endl;
  }
  return 0;
}

int verifyDirectory(){
  verifyDirectoryEntries(26,26);
  return 0;
}

int verifyDirectoryEntries(int path_no,int root_dir){
  directory_inode dir_inode;
  string sbPath = path + "/fusedata." + ::to_string(path_no) ;
  cout << "Verifying fusedata." << ::to_string(path_no) << " Directory..." << endl ;
  readDirectory(sbPath,dir_inode);
  verifyLinkCnt(dir_inode);
  verifyTimes(dir_inode.atime,dir_inode.ctime,dir_inode.mtime);
  verifyDotEntry(dir_inode.inode_dict,path_no,root_dir);
  vector<filename_to_inode_dict> subDir;
  findSubDirs(dir_inode.inode_dict,subDir);
  vector<filename_to_inode_dict> files;
  findFiles(dir_inode.inode_dict,files);
  for(vector<filename_to_inode_dict>::const_iterator i = subDir.begin(); i != subDir.end();++i){
    verifyDirectoryEntries(i->location,path_no);
  }
  for(vector<filename_to_inode_dict>::const_iterator i = files.begin(); i != files.end();++i){
    verifyFiles(i->location);
  }
  return 0;
}

int verifyLinkCnt(struct directory_inode &dir_inode){
  if(dir_inode.linkcount != (int)dir_inode.inode_dict.size())
    cout << "Link Count Mis match Link Count: " << to_string(dir_inode.linkcount) << "Actual Links: " << to_string(dir_inode.inode_dict.size()) << endl;
  return 0;
}

int verifyTimes(long atime,long ctime,long mtime){
  if(atime > (long)time(NULL))
    cout << "atime is Invalid" << endl;
  if(ctime > (long)time(NULL))
    cout << "creation time is Invalid" << endl;
  if(mtime > (long)time(NULL))
    cout << "modified time is Invalid" << endl;

  return 0;
}

int readDirectory(string fileName,struct directory_inode &dir_inode){
  ifstream file;
  file.open(fileName);
  if (!file.good()){
    cout << "File Open Error: " << fileName << endl;
    return 1;
  }
  string buffer;
  string data;
  while(getline(file,buffer)){
    data += buffer;
  }
  data.erase( ::remove_if( data.begin(), data.end(), ::isspace ), data.end() ); //remove white spaces
  sscanf(data.c_str(),"{size:%d,uid:%d,gid:%d,mode:%d,atime:%lu,ctime:%lu,mtime:%lu,linkcount:%d,filename_to_inode_dict:{",&dir_inode.size,&dir_inode.uid,&dir_inode.gid,&dir_inode.mode,&dir_inode.atime,&dir_inode.ctime,&dir_inode.mtime,&dir_inode.linkcount);
  vector<string> temp;
  vector<string> tokens;
  Tokenize(data,tokens,"{");
  Tokenize(tokens.at(tokens.size() - 1),temp,"}");
  tokens.clear();
  Tokenize(temp.at(temp.size() - 1),tokens,",");
  for(vector<string>::const_iterator i= tokens.begin(); i != tokens.end();++i ){
	   vector<string> elem;
     filename_to_inode_dict fid;
     Tokenize(*i,elem,":");
     fid.type = elem.at(0)[0];
     fid.name = elem.at(1);
     fid.location = atoi(elem.at(2).c_str());
     dir_inode.inode_dict.push_back(fid);
  }
  return 0;
}

int verifyDotEntry(vector<filename_to_inode_dict> &list,int dotLoc,int dotdotLoc){
  bool dotPresent = false;
  bool dotdot = false;
  for(vector<filename_to_inode_dict>::const_iterator i = list.begin(); i != list.end();++i){
    if(i->name == "."){
      dotPresent = true;
      if(i->location != dotLoc)
        cout << "Directory . location mismatch " << ::to_string(i->location) << " instead of " << ::to_string(dotLoc) << endl;
    }else if(i->name == ".."){
      dotdot = true;
      if(i->location != dotdotLoc)
        cout << "Directory .. location mismatch " << ::to_string(i->location) << " instead of " << ::to_string(dotdotLoc) << endl;
    }
  }
  if(!(dotPresent && dotdot))
    cout << "Error: directory doesnt contain . and .. entries" << endl;
  return 0;
}

int findSubDirs(vector<filename_to_inode_dict> &full_list, vector<filename_to_inode_dict> &dir_list){
  for(vector<filename_to_inode_dict>::const_iterator i = full_list.begin(); i != full_list.end();++i){
    if(i->name != "." && i->name != ".." && i->type == 'd')
      dir_list.push_back(*i);
  }
  return 0;
}

int findFiles(vector<filename_to_inode_dict> &full_list, vector<filename_to_inode_dict> &files){
  for(vector<filename_to_inode_dict>::const_iterator i = full_list.begin(); i != full_list.end();++i){
    if(i->type == 'f')
    files.push_back(*i);
  }
  return 0;
}

int verifyFiles(int path_no){
  file_inode f_inode;
  string sbPath = path + "/fusedata." + ::to_string(path_no) ;
  cout << "Verifying file fusedata." + ::to_string(path_no) << endl;
  readFileInode(sbPath,f_inode);
  //cout << ::to_string(f_inode.location) << " " << ::to_string(f_inode.indirect) << endl;
  verifyTimes(f_inode.atime,f_inode.ctime,f_inode.mtime);
  vector<int> indexes;
  if(f_inode.indirect == 1){
    getIndirectArray(f_inode.location,indexes);
    if(indexes.size() == 0)
      cout << "Invalid Indirect flag - The indirect location contains no array" << endl;
    if(indexes.size() == 1)
      cout << "Warning: Unnecessary use of Index Block. Indexed location Only One" << endl;
  }
  verifyFileSizeField(f_inode,indexes);

  return 0;
}

int verifyFileSizeField(struct file_inode &f_inode,vector<int> &indexes){
  int blockSize = 4096;
  if(!(f_inode.size > 0))
    cout << "Invalid file Size" << endl ;
  else if(f_inode.indirect == 0 && f_inode.size > blockSize)
    cout << "Error: Indirect is 0 and file size is greater than block size: " << ::to_string(f_inode.size) << endl;
  else if(f_inode.indirect != 0 && indexes.size() == 1){
    if(f_inode.size > blockSize)
        cout << "File Size is greater than indexed number of blocks" << endl;
  }
  else if(f_inode.indirect != 0 && indexes.size() > 1){
    int sizeMax = blockSize * indexes.size();
    if(f_inode.size > sizeMax)
      cout << "Error: size: " << ::to_string(f_inode.size) << " is > Max size: " << ::to_string(f_inode.size) << " for " << ::to_string(indexes.size()) << "Blocks" << endl;
    else if(f_inode.size < (int)(blockSize * (indexes.size() - 1)))
    cout << "Error: More blocks alloted than the required" << endl;
  }
  return 0;
}

int getIndirectArray(int index_no,vector<int> &indexes){
  string sbPath = path + "/fusedata." + ::to_string(index_no) ;
  ifstream file;
  file.open(sbPath);
  if (!file.good()){
    cout << "File Open Error: " << sbPath << endl;
    return 1;
  }
  getFreeBlockVector(file,indexes); //function reused where , seperated values are read
  //for(vector<int>::const_iterator i = indexes.begin(); i != indexes.end(); ++i)
    //cout << ::to_string(*i) << endl;
  return 0;
}

int readFileInode(string fileName,struct file_inode &f_inode){
  ifstream file;
  file.open(fileName);
  if (!file.good()){
    cout << "File Open Error: " << fileName << endl;
    return 1;
  }
  string buffer;
  string data;
  while(getline(file,buffer)){
    data += buffer;
  }
  data.erase( ::remove_if( data.begin(), data.end(), ::isspace ), data.end() ); //remove white spaces
  sscanf(data.c_str(),"{size:%d,uid:%d,gid:%d,mode:%d,linkcount:%d,atime:%lu,ctime:%lu,mtime:%lu,indirect:%dlocation:%d}",&f_inode.size,&f_inode.uid,&f_inode.gid,&f_inode.mode,&f_inode.linkcount,&f_inode.atime,&f_inode.ctime,&f_inode.mtime,&f_inode.indirect,&f_inode.location);
  return 0;
}

void Tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}
