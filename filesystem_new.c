/*
HW2
Dhishan Amaranath
N16909360
da1683

This file system needs the adjustment in the d_path variable to work correctly
*/
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

static const char *d_path = "/home/dhishan/Documents/Project/fusedata";

typedef struct{
	int creationTime;
	int mountCnt;
	int deviceID;
	int freeStart;
	int freeEnd;
	int root;
	int maxBlocks;
}superBlock;

typedef struct{
	char t;
	char name[256];
	int blockNumber;
}filename_to_inode_dict;


typedef struct{
	int size;
	int uid;
	int gid;
	int mode;
	long atime;
	long ctime;
	long mtime;
	int linkcount;
	filename_to_inode_dict inode_dict[10000];
}directory_inode;

static directory_inode root_directory;

typedef struct{
	int size;
	int uid;
	int gid;
	int mode;
	int linkcount;
	long atime;
	long ctime;
	long mtime;
	int indirect;
	int location;
}file_inode;

static int d_open(const char *path, struct fuse_file_info *fi)
{
	int cnt =0;
	char dir[256]="";
	char *parth_parse;
	strcpy(dir,path);
	parth_parse = strtok(dir,"/");
	fprintf(stderr,"%s parsed \n",parth_parse);
	//fprintf(stderr,"%d\n",(int)(sizeof(root_directory.inode_dict)/sizeof(root_directory.inode_dict[0])));
	for(cnt =0;cnt<root_directory.linkcount;cnt++){
		if(strcmp(root_directory.inode_dict[cnt].name,parth_parse) == 0){
			fi->fh = root_directory.inode_dict[cnt].blockNumber;
			fi->direct_io= 1;
 			fi->keep_cache= 1;
 			fi->flush= 1;
 			fi->nonseekable= 1;
 			fi->padding=27;
			return 0;

			}
		}
		return -ENOENT;
}

void getFileName(int blk_num,char* file_name)
{
	char block_prefix[256]="";
	//char block_Name[256]="";
	char *tmp = "/fusedata.";
	strcpy(block_prefix,d_path);
	strcat(block_prefix,tmp);
	sprintf(file_name,"%s%d",block_prefix,blk_num);
	//return block_Name;
}

int get_next_freeBlock()
{
	int block = 0;
	int blockCnt =1;
	char block_prefix[256]="";
  char block_Name[256]="";
	char *tmp = "/fusedata.";
  strcpy(block_prefix,d_path);
	strcat(block_prefix,tmp);
  FILE *fp;
	for(blockCnt =1;blockCnt<=25;blockCnt++)
	{
		sprintf(block_Name,"%s%d",block_prefix,blockCnt);
		char data[4096];
		char *part;
		fp=fopen(block_Name,"r");
		if(fp)
		{
			fprintf(stderr,"opened %s file",block_Name);
			fgets(data,4096,fp);
			//fprintf(stderr,"%s",data);
			if(data[0] == EOF)
			{
				fprintf(stderr,"EOF reached?\n");
				continue;
			}
			sscanf(data,"%d,",&block);
			part = strtok(data,",");
		}
		fclose(fp);
		fp = fopen(block_Name,"w");
		if(fp)
		{
			part = strtok(NULL,",");
			while(part != NULL)
			{

				fprintf(fp,"%s,",part);
				part = strtok(NULL,",");
			}
			fseek(fp,-2,SEEK_END);
			fprintf(fp,'\0');
		}
		fclose(fp);

		break;
	}

	return block;
}

int read_file_inode(char *name,file_inode *f_inode){
	FILE *fp;
	fp = fopen(name,"r");
	if(fp)
	{
		fscanf(fp,"{size:%d,uid:%d,gid:%d,mode:%d,linkcount:%d,atime:%lu,ctime:%lu,mtime:%lu,indirect:%d,location:%d}",&f_inode->size,&f_inode->uid,&f_inode->gid,&f_inode->mode,&f_inode->linkcount,&f_inode->atime,&f_inode->ctime,&f_inode->mtime,&f_inode->indirect,&f_inode->location);
		fclose(fp);//while(fgets(line,100,fp) != NULL);
	}
	return 0;
}

int read_dir_inode(char *name,directory_inode *dir_inode){

	fprintf(stderr,"test");
	char data[4096];
	char *part;
	FILE *fp;
	fp =fopen(name,"r");
	if(fp)
	{
		fgets(data,4096,fp);
	}
	fclose(fp);
	sscanf(data,"{size:%d,uid:%d,gid:%d,mode:%d,atime:%lu,ctime:%lu,mtime:%lu,linkcount:%d,filename_to_inode_dict:{",&dir_inode->size,&dir_inode->uid,&dir_inode->gid,&dir_inode->mode,&dir_inode->atime,&dir_inode->ctime,&dir_inode->mtime,&dir_inode->linkcount);
	part = strtok(data,"{");
	part = strtok(NULL,"{");
	part = strtok(part,"}");
	char *dict_nodes[dir_inode->linkcount +1];
	int cnt=0;
	dict_nodes[0] = strtok(part,",");
	while(dict_nodes[cnt++] != NULL){
		dict_nodes[cnt] = strtok(NULL,",");
	}
	int cntr =0;
	//fprintf(stderr,"this is @ read dir inode %d\t%d\n",cnt,dir_inode->linkcount);
	for(cntr =0;cntr<cnt-1;cntr++)
	{
		dir_inode->inode_dict[cntr].t = strtok(dict_nodes[cntr],":")[0];
		fprintf(stderr,"%c\n",dir_inode->inode_dict[cntr].t);

		strcpy(dir_inode->inode_dict[cntr].name,strtok(NULL,":"));
		dir_inode->inode_dict[cntr].blockNumber = atoi(strtok(NULL,":"));
		fprintf(stderr,"%s\n",dir_inode->inode_dict[cntr].name);
		fprintf(stderr,"%d\n",dir_inode->inode_dict[cntr].blockNumber);
		fprintf(stderr,"\n\nthis is for reading the directory entries\n");

	}
	fprintf(stderr,"returning at %d\n",cntr);
	return 0;
}

int write_file_inode(char *file_inode_name,file_inode *f_inode){
	FILE *fp;
	fp = fopen(file_inode_name,"w+");
	if(fp){
		fprintf(fp,"{size:%d,uid:%d,gid:%d,mode:%d,linkcount:%d,atime:%lu,ctime:%lu,mtime:%lu,indirect:%d,location:%d}",f_inode->size,f_inode->uid,f_inode->gid,f_inode->mode,f_inode->linkcount,f_inode->atime,f_inode->ctime,f_inode->mtime,f_inode->indirect,f_inode->location);
		fclose(fp);
	}
	return 0;
}

int write_directory_inode(directory_inode *root_inode,char *Dir_name){
	FILE *fp;
	fprintf(stderr," write_directory_inode \n");
  fp = fopen(Dir_name,"w+");
	if(fp)
	{
		fprintf(fp,"{size:%d,uid:%d,gid:%d,mode:%d,atime:%lu,ctime:%lu,mtime:%lu,linkcount:%d,filename_to_inode_dict:{",root_inode->size,root_inode->uid,root_inode->gid,root_inode->mode,root_inode->atime,root_inode->ctime,root_inode->mtime,root_inode->linkcount);
		int cnt =0;
		for(cnt =0; cnt < root_inode->linkcount ;cnt++)
		{
			if(cnt!=0)
				fprintf(fp,",");
			fprintf(fp,"%c:%s:%d",root_inode->inode_dict[cnt].t,root_inode->inode_dict[cnt].name,root_inode->inode_dict[cnt].blockNumber);
		}
		fprintf(fp,"}}");
		fclose(fp);
	}
	return 1;
}

static int d_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;

	//if (strcmp(path, "/") != 0)
		//return -ENOENT;

	//filling the buf with the contents of the directory
	int cnt =0;

	if(strcmp(path,"/")==0)
	{
		for(cnt = 0;cnt<root_directory.linkcount;cnt++){
			filler(buf,root_directory.inode_dict[cnt].name,NULL,0);
		}
		return 0;

	}
	else{
		char dir[256]="";
		char *parth_parse;
		strcpy(dir,path);
		parth_parse = strtok(dir,"/");
		for(cnt =0;cnt<root_directory.linkcount;cnt++){
			if(strcmp(root_directory.inode_dict[cnt].name,parth_parse) == 0){
				fprintf(stderr,"true @ getattr %d\n",cnt);
				if(root_directory.inode_dict[cnt].t == 'd'){
					char block_Name[256] = "";
					getFileName(root_directory.inode_dict[cnt].blockNumber,block_Name);
					directory_inode existing_inode;
					read_dir_inode(block_Name,&existing_inode);
					for(cnt = 0;cnt<existing_inode.linkcount;cnt++){
						filler(buf,existing_inode.inode_dict[cnt].name,NULL,0);
					}
					return 0;

				}
			}
		}
	}
	//filler(buf, ".", NULL, 0);
	//filler(buf, "..", NULL, 0);
	//filler(buf, hello_path + 1, NULL, 0);

	return 0;
}

static int d_getattr(const char *path, struct stat *stbuf)
{
	int res = 0;
	fprintf(stderr,"%s\nentered\n",path);
	memset(stbuf, 0, sizeof(struct stat));
	if(strcmp(path,"/")==0)
	{
		stbuf->st_size = root_directory.size;
		stbuf->st_uid = root_directory.uid;
		stbuf->st_gid = root_directory.gid;
		stbuf->st_mode = root_directory.mode;
		stbuf->st_ctime = (int)root_directory.ctime;
		stbuf->st_atime = (int)root_directory.atime;
		stbuf->st_mtime = (int)root_directory.mtime;
		stbuf->st_nlink = root_directory.linkcount;
		return res;
	}
	else{
		int cnt =0;
		char dir[256]="";
		char *parth_parse;
		strcpy(dir,path);
		parth_parse = strtok(dir,"/");
		fprintf(stderr,"%s parsed \n",parth_parse);
		//fprintf(stderr,"%d\n",(int)(sizeof(root_directory.inode_dict)/sizeof(root_directory.inode_dict[0])));
		for(cnt =0;cnt<root_directory.linkcount;cnt++){
			if(strcmp(root_directory.inode_dict[cnt].name,parth_parse) == 0){
				fprintf(stderr,"true @ getattr %d\n",cnt);
				if(root_directory.inode_dict[cnt].t == 'd'){
					char block_Name[256] = "";
					getFileName(root_directory.inode_dict[cnt].blockNumber,block_Name);
					directory_inode existing_inode;
					read_dir_inode(block_Name,&existing_inode);
					stbuf->st_size = existing_inode.size;
					stbuf->st_uid = existing_inode.uid;
					stbuf->st_gid = existing_inode.gid;
					stbuf->st_mode = existing_inode.mode;
					stbuf->st_ctime = (int)existing_inode.ctime;
					stbuf->st_atime = (int)existing_inode.atime;
					stbuf->st_mtime = (int)existing_inode.mtime;
					stbuf->st_nlink = existing_inode.linkcount;
					fprintf(stderr,"returned new stat sucessfully %d\n",cnt);

						return res;
				}else if(root_directory.inode_dict[cnt].t == 'f')
				{
					char block_Name[256] = "";
					getFileName(root_directory.inode_dict[cnt].blockNumber,block_Name);
					file_inode f_inode;
					read_file_inode(block_Name,&f_inode);
					stbuf->st_size = f_inode.size;
					stbuf->st_uid = f_inode.uid;
					stbuf->st_gid = f_inode.gid;
					stbuf->st_mode = f_inode.mode;
					stbuf->st_ctime = (int)f_inode.ctime;
					stbuf->st_atime = (int)f_inode.atime;
					stbuf->st_mtime = (int)f_inode.mtime;
					stbuf->st_nlink = f_inode.linkcount;
					return res;
				}

				return res;
				//TODO:go to the directory location if the path is a directory and fill up stat structure from there
				//TODO:if its a file return this directory attributes in stat
				//TODO:split the path for subdirectories and the do the same
			}
		}
	}
	res = -ENOENT;
	return res;
}

//append field- 1 for append
//0 for delete
//appends or deletes the inode_entry
int update_directoryInode(char *Dir_name,int append,filename_to_inode_dict inode_entry){
	if(append)
	{
		char dir[256];
		strcpy(dir,Dir_name);
		fprintf(stderr,"update directory inode");
		directory_inode existing_inode;
		read_dir_inode(dir,&existing_inode);
		existing_inode.inode_dict[existing_inode.linkcount].t = inode_entry.t;
		strcpy(existing_inode.inode_dict[existing_inode.linkcount].name,inode_entry.name);
		existing_inode.inode_dict[existing_inode.linkcount].blockNumber = inode_entry.blockNumber;
		existing_inode.linkcount++;
		existing_inode.mtime = (long)time(NULL);
		fprintf(stderr," after read %s\n",dir);
		write_directory_inode(&existing_inode,dir);
		fprintf(stderr,"updated directory entry");
		return 0;
	}
	return -1;
}

static int d_mkdir(const char *path, mode_t mode)
{
    //get the next free block
		int free_Blk;
		free_Blk = get_next_freeBlock();
		if(free_Blk == 0){
		fprintf(stderr,"no free blocks left");
		return -1;
		}
		fprintf(stderr,"entered mkdir %d\n",free_Blk);
		//make the directory entries in the new block
		directory_inode dir_inode = {0,1,1,16877,(long)time(NULL),(long)time(NULL),(long)time(NULL),2,{{'d',".",free_Blk},{'d',"..",26}}};
		char block_Name[256] = "";
		getFileName(free_Blk,block_Name);
		fprintf(stderr,"%s",block_Name);
		write_directory_inode(&dir_inode,block_Name);
		fprintf(stderr,"%s\n",path);
		char name[256]="";
		char *parth_parse;
		strcpy(name,path);
		parth_parse = strtok(name,"/");
		fprintf(stderr,"%s\n",parth_parse);
		//update the inode at the previous diretory file - in our case only one level - root directory
		filename_to_inode_dict inode_new;// = {'d',"test",free_Blk};
		inode_new.t = 'd';
		strcpy(inode_new.name,parth_parse);
		inode_new.blockNumber = free_Blk;
		char block_Name_root[256] = "";
		getFileName(26,block_Name_root);
		update_directoryInode(block_Name_root,1,inode_new);
		//update the cache directly or from the file
		root_directory.inode_dict[root_directory.linkcount] = inode_new;
		root_directory.linkcount++;

    return 0;
}


int edit_mount_count(char *blockName)
{
		FILE *fp;
		//char line[100];
		fp = fopen(blockName,"r");
		superBlock sBlock;
		if(fp)
		{
			fscanf(fp,"{creationTime:%d,mounted:%d,devId:%d,freeStart:%d,freeEnd:%d,root:%d,maxBlocks:%d}",&sBlock.creationTime,&sBlock.mountCnt,&sBlock.deviceID,&sBlock.freeStart,&sBlock.freeEnd,&sBlock.root,&sBlock.maxBlocks);
			//while(fgets(line,100,fp) != NULL);
			fprintf(stderr,"%d",sBlock.mountCnt);
			sBlock.mountCnt++;
			fprintf(stderr,"%d",sBlock.mountCnt);
		}
		fclose(fp);
		fp = fopen(blockName,"w");
		if(fp)
		{
			fprintf(fp,"{creationTime:%d,mounted:%d,devId:%d,freeStart:%d,freeEnd:%d,root:%d,maxBlocks:%d}",sBlock.creationTime,sBlock.mountCnt,sBlock.deviceID,sBlock.freeStart,sBlock.freeEnd,sBlock.root,sBlock.maxBlocks);
		}
		fclose(fp);
			return 1;
}

static int d_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
	//check whether file exists
	//else
	//get free block
	fprintf(stderr,"create file %s",path);
	int free_Blk;
	free_Blk = get_next_freeBlock();
	int file_Blk;
	file_Blk = get_next_freeBlock(); //assuming the file size of only 4096
	if(free_Blk == 0 || file_Blk == 0){
	fprintf(stderr,"no free blocks left");
	return -1;
	}
	//create an file inode block
	file_inode f_inode = {4096,1,1,33261,2,(long)time(NULL),(long)time(NULL),(long)time(NULL),0,file_Blk};
	char block_Name[256] = "";
	getFileName(free_Blk,block_Name);
	write_file_inode(block_Name,&f_inode);
	//update the directory inode
	char name[256]="";
	char *parth_parse;
	strcpy(name,path);
	parth_parse = strtok(name,"/");
	filename_to_inode_dict inode_new;
	inode_new.t = 'f';
	strcpy(inode_new.name,parth_parse);
	inode_new.blockNumber = free_Blk;
	char block_Name_root[256] = "";
	getFileName(26,block_Name_root);
	update_directoryInode(block_Name_root,1,inode_new);
	root_directory.inode_dict[root_directory.linkcount] = inode_new;
	root_directory.linkcount++;
	return 0;	//return -ENOENT;
}

//Create 10,000 files in file system with names fusedata.X [0-9,999].
//Superblock at 0th index with name fusedata.0
//25 consecutive blocks (files) containing free block table with each containing 400 block indexes except for the first which is 373
//26th block is root direcory
//All other files with 4096 bytes of 0 written inside them
void *d_init(const char *file_info_conn){

	fprintf(stderr,"Starting writing Super Block\n");
  superBlock sBlock = {(int)time(NULL),1,20,1,25,26,10000};
  char block_prefix[256]="";
  char block_Name[256]="";
  strcpy(block_prefix,d_path);
	fprintf(stderr,"%s\n",block_prefix);
	char *tmp = "/fusedata.";
  strcat(block_prefix,tmp);
  //printf("%s",block_prefix);
	fprintf(stderr,"%s\n",block_prefix);
  sprintf(block_Name,"%s%d",block_prefix,0);
  //strcat(block_Name,".0");
	fprintf(stderr,"%s\n",block_Name);
  FILE *fp;
	fp = fopen(block_Name,"r");
	if(!fp)
	{
	  fp = fopen(block_Name,"w");
	  if(fp)
	  {
	    fprintf(fp,"{creationTime:%d,mounted:%d,devId:%d,freeStart:%d,freeEnd:%d,root:%d,maxBlocks:%d}",sBlock.creationTime,sBlock.mountCnt,sBlock.deviceID,sBlock.freeStart,sBlock.freeEnd,sBlock.root,sBlock.maxBlocks);
	    fclose(fp);
			fprintf(stderr,"success writing Super Block\n");

	  }
	  int blockCnt =0;
	  int freeBlkCnt = 0;
		fprintf(stderr,"writing into the first file\n");//
	  sprintf(block_Name,"%s%d",block_prefix,1);
		fprintf(stderr,"%s\n",block_Name);
	  fp = fopen(block_Name,"w");
	  if(fp)
	  {
	    for(freeBlkCnt = 27;freeBlkCnt<400;freeBlkCnt++)
	      fprintf(fp,"%d,",freeBlkCnt);
	    fclose(fp);
			fprintf(stderr,"writing into the fusedata1 Success\n");//

	  }

	  for(blockCnt = 2;blockCnt<=25;blockCnt++)
	  {
	    sprintf(block_Name,"%s%d",block_prefix,blockCnt);
			fprintf(stderr,"writing into the %s\n",block_Name);//

	    fp = fopen(block_Name,"w");
	    if(fp)
	    {
	      for(freeBlkCnt = 0;freeBlkCnt < 400;freeBlkCnt++ )
	      {
	        fprintf(fp,"%d,",(blockCnt -1)*400 + freeBlkCnt);
	      }
	    }
	    fclose(fp);
	    printf("writing to fusedata.%d success\n",blockCnt);
	  }
		fprintf(stderr,"Creating Root Directory\n");
		sprintf(block_Name,"%s%d",block_prefix,26);
		//filename_to_inode_dict root_dict = {'d','.',sBlock.root};
		//filename_to_inode_dict root_dict1 = {'d',"..",sBlock.root};
		directory_inode root_inode = {0,1,1,16877,(long)time(NULL),(long)time(NULL),(long)time(NULL),2,{{'d',".",sBlock.root},{'d',"..",sBlock.root}}};
		write_directory_inode(&root_inode,block_Name);
		//root_directory = root_inode;
		//create blocks total 10,000 -temporary 200
		for(blockCnt = 27;blockCnt < 200;blockCnt++)
		{
			sprintf(block_Name,"%s%d",block_prefix,blockCnt);
			fp = fopen(block_Name,"w");
	    if(fp)
	    {
	      for(freeBlkCnt = 0;freeBlkCnt < 4096;freeBlkCnt++ )
	      {
	        fprintf(fp,"%d",0);
	      }
	    }
	    fclose(fp);
		}
		read_dir_inode(block_Name,&root_directory);
	}
	else
	{
		fclose(fp);
		edit_mount_count(block_Name);
		sprintf(block_Name,"%s%d",block_prefix,26);
		read_dir_inode(block_Name,&root_directory);
	}

 return NULL;
}


static struct fuse_operations d_operations = {
	.init 		= d_init,
	.getattr	= d_getattr,
	.readdir	= d_readdir,
	.open		= d_open,
	.mkdir		= d_mkdir,
	.create 		= d_create
};

int main(int argc, char *argv[])
{
	return fuse_main(argc, argv, &d_operations, NULL);
}
